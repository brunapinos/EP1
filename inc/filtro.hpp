#ifndef FILTRO_HPP
#define FILTRO_HPP

#include <iostream>
#include <string>
#include <fstream>

#include "funcionalidade.hpp"
#include "figura.hpp"

using namespace std;


class Filtro: public Funcionalidade{

protected:
	Filtro();
	void AplicarFiltro();
	virtual void GravarArquivo(Figura *ptr_figura, Pixel *ptr_pixel);

};


#endif
