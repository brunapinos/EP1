#ifndef FIGURA_HPP
#define FIGURA_HPP

#include <string>

#include "funcionalidade.hpp"

#define Max 1020


using namespace std;

typedef struct Pixel{

	unsigned char vermelho[Max][Max];
	unsigned char verde[Max][Max];
	unsigned char azul[Max][Max];

}Pixel;

class Figura: public Funcionalidade{

private:
	string nome_arquivo;
	int largura;
	int altura;
	string tipo;
	int intensidade;


public:
	Figura();

	int getLargura();
	void setLargura(int largura);
	int getAltura();
	void setAltura(int altura);
	string  getTipo();
	void setTipo(string tipo);
	int getIntensidade_max();
	void setIntensidade_max(int intensidade);
	void setNome_arquivo(string nome_arquivo);
	string getNome_arquivo();
	Pixel* copiarInformacoes(string nome_arquivo);

};

#endif
