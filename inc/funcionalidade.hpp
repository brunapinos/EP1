#ifndef FUNCIONALIDADE_HPP
#define FUNCIONALIDADE_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

using namespace std;

class Funcionalidade{

public:
	int validaOpcao(int num_opcoes);
	void limparTela();
	string validaString();

};


#endif
