#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP

#include <iostream>
#include <string>
#include <fstream>
#include "filtro.hpp"


using namespace std;


class Negativo : public Filtro{

protected:
	Negativo();
	void GravarArquivo(Figura *ptr_figura, Pixel *ptr_pixel);
        Pixel* AplicarFiltro(Figura *ptr_figura, Pixel *ptr_pixel, Pixel *ptr_novo);

};

#endif
