#ifndef MEDIA_HPP
#define MEDIA_HPP

#include <iostream>
#include <string>
#include <fstream>
#include "filtro.hpp"


using namespace std;


class Media : public Filtro{

protected:

	Media();
        Pixel* AplicarFiltro(Figura *ptr_figura, Pixel *ptr_pixel, Pixel *ptr_novo);
	void GravarArquivo(Figura *ptr_figura, Pixel *ptr_pixel);

};

#endif
