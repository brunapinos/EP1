#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP

#include <iostream>
#include <string>
#include <fstream>
#include "filtro.hpp"


using namespace std;


class Polarizado : public Filtro{

protected:
	void GravarArquivo(Figura *ptr_figura, Pixel *ptr_pixel);
        Pixel* AplicarFiltro(Figura *ptr_figura, Pixel *ptr_pixel, Pixel *ptr_novo);

};

#endif
