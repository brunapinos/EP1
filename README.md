# EP1 - OO (UnB - Gama)

# Orientação a Objetos 2/2016

# Bruna Pinos 15/0119984

# Programa de Aplicação de filtro em imagens


** Este programa aplica filtros em imagens PPM do tipo P6.

** Garanta que ao iniciar este programa a imagem que deseja
altera se encontre na pasta 'doc' deste programa.

** Caso não sabia se a imagem é do tipo P6, o programa irá informar.

** As imagens alteradas se encontrarão na pasta 'doc' deste arquivo.

# Como compilar e executar

	*Para executar este programa, primeiro abra o terminal do
	seu computador com o sistema operacional Linux;
	* Encontre o diretorio raiz e abra-o;
	* Para limpar os arquivos objetos digite "make clean";
	* Para compilar digite "make";
	* Para rodar o programa digite "make run";	
