#include <iostream>
#include "pretobranco.hpp"
#include <string>
#include <fstream>
#include <stdlib.h>

#define Max 1020


using namespace std;

PretoBranco:: PretoBranco(){}

Pixel* PretoBranco:: AplicarFiltro(Figura *ptr_figura, Pixel *ptr_pixel, Pixel *ptr_novo){

        int vermelho[Max][Max], azul[Max][Max], verde[Max][Max];
	int largura = ptr_figura -> getLargura();
	int altura = ptr_figura ->getAltura();
	int intensidade = ptr_figura->getIntensidade_max();
	int cinza;

        for(int i = 0; i < altura; ++i){

           for(int j = 0; j < largura; ++j){

		vermelho[i][j] = (int) ptr_pixel->vermelho[i][j];
		verde[i][j] = (int) ptr_pixel->verde[i][j];
		azul[i][j] = (int) ptr_pixel->azul[i][j];

		cinza = (0.299 * vermelho[i][j]) + (0.587 * verde[i][j]) + (0.144 * azul[i][j]);
		if(cinza > intensidade){
		  cinza = intensidade;
		}
                ptr_novo->vermelho[i][j] = cinza;
                ptr_novo->verde[i][j] = cinza;
                ptr_novo->azul[i][j] = cinza;

                }
          }

return ptr_novo;
}


void PretoBranco:: GravarArquivo(Figura *ptr_figura, Pixel *ptr_pixel){

        cout<<"Iniciando gravação do arquivo"<<endl;

        ofstream ponteiro_copia;

        string conteudo;

        conteudo = ptr_figura->getNome_arquivo();

        string nome_copia = "./doc/pretobranco" + conteudo;
        ponteiro_copia.open(nome_copia.c_str());

        ponteiro_copia << ptr_figura -> getTipo() << endl;
        ponteiro_copia << ptr_figura -> getLargura() << endl;
        ponteiro_copia << ptr_figura -> getAltura() << endl;
        ponteiro_copia << ptr_figura -> getIntensidade_max() << endl;



        for(int i=0; i < ptr_figura->getAltura(); i++){

          for(int j=0; j < ptr_figura->getLargura(); j++){

                ponteiro_copia.write((char *)(&ptr_pixel->vermelho[i][j]),1);
                ponteiro_copia.write((char *)(&ptr_pixel->verde[i][j]),1);
                ponteiro_copia.write((char *)(&ptr_pixel->azul[i][j]),1);

                }
        }

        ponteiro_copia.close();

	limparTela();
        cout <<"Gravação realizada com sucesso!" << endl;
        cout <<"Filtro aplicado" << endl;
        cout <<"A imagem se encontra na pasta 'doc' deste programa." <<endl;
}

