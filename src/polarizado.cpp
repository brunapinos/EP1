#include <iostream>
#include "polarizado.hpp"
#include <string>
#include <fstream>
#include <stdlib.h>

#define Max 1020


using namespace std;

Pixel* Polarizado:: AplicarFiltro(Figura *ptr_figura, Pixel *ptr_pixel, Pixel *ptr_novo){

	int intensidade, largura, altura;
	int vermelho[Max][Max], azul[Max][Max], verde[Max][Max];


	intensidade = ptr_figura -> getIntensidade_max();
	largura = ptr_figura -> getLargura();
	altura = ptr_figura -> getAltura();


     for(int i = 0; i < altura; ++i){

	 for(int j = 0; j < largura; ++j){

		vermelho[i][j] = (int) ptr_pixel->vermelho[i][j];
		verde[i][j] = (int)ptr_pixel->verde[i][j];
		azul[i][j] = (int)ptr_pixel->azul[i][j];


	    if(vermelho[i][j] < intensidade/2){
		ptr_novo-> vermelho[i][j] = 0;
		}
	    else{
		    ptr_novo-> vermelho[i][j] = intensidade;
 		  }


            if(verde[i][j] < intensidade/2){
                ptr_novo-> verde[i][j] = 0;
               }
            else{
                    ptr_novo-> verde[i][j] = intensidade;
                  }


            if(azul[i][j] < intensidade/2){
                ptr_novo-> azul[i][j] = 0;
	      }
            else{
                    ptr_novo-> azul[i][j] = intensidade;
                  }

		}
          }


return ptr_novo;
}


void Polarizado:: GravarArquivo(Figura *ptr_figura, Pixel *ptr_pixel){

        cout<<"Iniciando gravação do arquivo"<<endl;

        ofstream ponteiro_copia;

        string conteudo;

	conteudo = ptr_figura->getNome_arquivo();

        string nome_copia = "./doc/polarizado" + conteudo;
        ponteiro_copia.open(nome_copia.c_str());

        ponteiro_copia << ptr_figura -> getTipo() << endl;
        ponteiro_copia << ptr_figura -> getLargura() << endl;
        ponteiro_copia << ptr_figura -> getAltura() << endl;
        ponteiro_copia << ptr_figura -> getIntensidade_max() << endl;



        for(int i=0; i < ptr_figura->getAltura(); i++){

          for(int j=0; j < ptr_figura->getLargura(); j++){

                ponteiro_copia.write((char *)(&ptr_pixel->vermelho[i][j]),1);
                ponteiro_copia.write((char *)(&ptr_pixel->verde[i][j]),1);
                ponteiro_copia.write((char *)(&ptr_pixel->azul[i][j]),1);

                }
        }

        ponteiro_copia.close();
        cout <<"Gravação realizada com sucesso!" << endl;
        cout <<"Filtro aplicado" << endl;


}
