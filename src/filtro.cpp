#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

#include "filtro.hpp"


#define Max 1020


using namespace std;

Filtro:: Filtro(){}

void Filtro:: GravarArquivo(Figura *ptr_figura, Pixel *ptr_pixel){


        cout<<"Iniciando gravação do arquivo"<<endl;

        ofstream ponteiro_copia;

        string conteudo;

        conteudo = ptr_figura->getNome_arquivo();

        string nome_copia = "./doc/filtrado" + conteudo;

        ponteiro_copia.open(nome_copia.c_str());

	ponteiro_copia << ptr_figura -> getTipo() << endl;
	ponteiro_copia << ptr_figura -> getLargura() << endl;
	ponteiro_copia << ptr_figura -> getAltura() << endl;
	ponteiro_copia << ptr_figura -> getIntensidade_max() << endl;


	for(int i=0; i < (ptr_figura->getLargura()); i++){

	  for(int j=0; j < (ptr_figura->getAltura()); j++){

                ponteiro_copia.write((char *)(&ptr_pixel->vermelho[i][j]),1);
                ponteiro_copia.write((char *)(&ptr_pixel->verde[i][j]),1);
                ponteiro_copia.write((char *)(&ptr_pixel->azul[i][j]),1);

		}
	}

	ponteiro_copia.close();

	limparTela();
        cout <<"Gravação realizada com sucesso!" << endl;
	cout <<"Filtro aplicado." << endl;
	cout <<"A imagem se encontra na pasta 'doc' deste programa." << endl;
}
