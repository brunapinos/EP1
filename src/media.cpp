#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>

#include "media.hpp"



using namespace std;

Media:: Media(){}

Pixel* Media:: AplicarFiltro(Figura *ptr_figura, Pixel *ptr_pixel, Pixel *ptr_novo){

  int opcao;
  int auxr = 0, auxg = 0, auxb = 0;

cout << "Entre com a opcao da matriz da media desejada: " << endl;
cout << "1. 3x3			2. 5x5" <<endl;
cout << "3. 7x7" <<endl;
opcao = validaOpcao(3);

switch(opcao){

case 1:

 for(int i=0; i < (ptr_figura->getAltura()); ++i){
  for(int j=0; j < (ptr_figura->getLargura()); ++j){

    ptr_novo->vermelho[i][j] = ptr_pixel->vermelho[i][j];
    ptr_novo->verde[i][j] = ptr_pixel->verde[i][j];
    ptr_novo->azul[i][j] = ptr_pixel->azul[i][j];

  }
 }


 for(int i = 1; i< (ptr_figura->getAltura()-1); ++i){
  for(int j = 1; j < (ptr_figura->getLargura()-1); ++j){

    auxr= 0; auxg=0; auxb=0;


  for(int k = i-1; k <= i+1; ++k){
    for(int x = j-1; x <= j+1; ++x){

     auxr = auxr + (int) ptr_novo->vermelho[k][x];
     auxg = auxg + (int)ptr_novo->verde[k][x];
     auxb = auxb + (int)ptr_novo->azul[k][x];
    }
  }


  ptr_novo->vermelho[i][j] = auxr/9;
  ptr_novo->verde[i][j] =  auxg/9;
  ptr_novo->azul[i][j] =  auxb/9;

  }
 }

break;

case 2:
 for(int i=0; i < (ptr_figura->getAltura()); ++i){
   for(int j=0; j < (ptr_figura->getLargura()); ++j){

     ptr_novo->vermelho[i][j] = ptr_pixel->vermelho[i][j];
     ptr_novo->verde[i][j] = ptr_pixel->verde[i][j];
     ptr_novo->azul[i][j] = ptr_pixel->azul[i][j];

   }
 }

 for(int i = 2; i< (ptr_figura->getAltura()-2); ++i){
  for(int j = 2; j < (ptr_figura->getLargura()-2); ++j){

    auxr= 0; auxg=0; auxb=0;

  for(int k = i-2; k <= i+2; ++k){
    for(int x = j-2; x <= j+2; ++x){

     auxr = auxr +(int) ptr_novo->vermelho[k][x];
     auxg = auxg + (int)ptr_novo->verde[k][x];
     auxb = auxb + (int)ptr_novo->azul[k][x];
   }
  }


  ptr_novo->vermelho[i][j] = auxr/25;
  ptr_novo->verde[i][j] =  auxg/25;
  ptr_novo->azul[i][j] =  auxb/25;

  }
 }

break;

case 3:
 for(int i=0; i < (ptr_figura->getAltura()); ++i){
  for(int j=0; j < (ptr_figura->getLargura()); ++j){

    ptr_novo->vermelho[i][j] = ptr_pixel->vermelho[i][j];
    ptr_novo->verde[i][j] = ptr_pixel->verde[i][j];
    ptr_novo->azul[i][j] = ptr_pixel->azul[i][j];

   }
 }

for(int i = 3; i< (ptr_figura->getAltura()-3); ++i){
 for(int j = 3; j < (ptr_figura->getLargura()-3); ++j){

   auxr= 0; auxg=0; auxb=0;


 for(int k = i-3; k <= i+3; ++k){
   for(int x = j-3; x <= j+3; ++x){

     auxr = auxr +(int) ptr_novo->vermelho[k][x];
     auxg = auxg + (int)ptr_novo->verde[k][x];
     auxb = auxb + (int)ptr_novo->azul[k][x];
   }
 }


ptr_novo->vermelho[i][j] = auxr/49;
ptr_novo->verde[i][j] =  auxg/49;
ptr_novo->azul[i][j] =  auxb/49;

   }
 }

break;
}

return ptr_novo;
}


void Media:: GravarArquivo(Figura *ptr_figura, Pixel *ptr_pixel){

        cout<<"Iniciando gravação do arquivo"<<endl;

        ofstream ponteiro_copia;

        string conteudo;
	int opcao;
	string media;

	conteudo = ptr_figura->getNome_arquivo();

	cout << "Digite novamente a opcao do filtro, para salvar o nome da imagem: " << endl;
	cout << "1. 3x3                 2. 5x5" <<endl;
	cout << "3. 7x7" <<endl;
	opcao = validaOpcao(3);

	if(opcao==1){
		media = "3x3";
	}
	else if(opcao==2){
		media = "5x5";
	}
	else{ media = "7x7";}

        string nome_copia = "./doc/" + media+ conteudo;
        ponteiro_copia.open(nome_copia.c_str());

        ponteiro_copia << ptr_figura -> getTipo() << endl;
        ponteiro_copia << ptr_figura -> getLargura() << endl;
        ponteiro_copia << ptr_figura -> getAltura() << endl;
        ponteiro_copia << ptr_figura -> getIntensidade_max() << endl;



        for(int i=0; i < ptr_figura->getAltura(); i++){

          for(int j=0; j < ptr_figura->getLargura(); j++){

                ponteiro_copia.write((char *)(&ptr_pixel->vermelho[i][j]),1);
                ponteiro_copia.write((char *)(&ptr_pixel->verde[i][j]),1);
                ponteiro_copia.write((char *)(&ptr_pixel->azul[i][j]),1);

                }
        }

        ponteiro_copia.close();

	limparTela();
        cout <<"Gravação realizada com sucesso!" << endl;
        cout <<"Filtro aplicado" << endl;
        cout <<"A imagem se encontra na pasta 'doc' deste programa." <<endl;

}
