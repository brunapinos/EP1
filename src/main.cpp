#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#include "funcionalidade.hpp"
#include "figura.hpp"
#include "pretobranco.hpp"
#include "negativo.hpp"
#include "polarizado.hpp"
#include "media.hpp"

using namespace std;


int main(int argc, char ** argv){

int contador;
Funcionalidade *ptr = new Funcionalidade;

Figura *ptr_figura = new Figura;
Pixel *ptr_pixel = new Pixel, *ptr_novo = new Pixel;

string nome;

	ptr->limparTela();

	cout << "\nBem Vindo ao Programa de Aplicacao de filtro." << endl;
	cout << "Digite a opcao de filtro de deseja aplicar." << endl;
	cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++\n" << endl;
	cout << " 1. Filtro Negativo \t 2. Filtro Preto e Branco " << endl;
	cout << " 3. Filtro Polarizado \t 4. Filtro de Média " << endl;
	cout << " 5. Sair " << endl;
	cout << "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;


	contador = ptr->validaOpcao(5);

switch(contador){

case 1:
{
	ptr->limparTela();
        cout << "Digite o nome do arquivo que deseja abrir." << endl;
        cout << "(A imagem deve estar na pasta 'doc' deste programa)" << endl;
        cout << "Exemplo: arquivo.ppm" << endl;
	ptr_figura->setNome_arquivo(ptr->validaString());

        ptr->limparTela();

	nome = ptr_figura->getNome_arquivo();
	ptr_pixel = ptr_figura -> copiarInformacoes(nome);

	Negativo *ptr_negativo = new Negativo;

	ptr_novo = ptr_negativo->AplicarFiltro(ptr_figura, ptr_pixel,ptr_novo);
	ptr_negativo->GravarArquivo(ptr_figura,ptr_novo);



}
	break;

case 2:
{
	ptr->limparTela();
        cout << "Digite o nome do arquivo que deseja abrir." << endl;
        cout << "(A imagem deve estar na pasta 'doc' deste programa)" << endl;
        cout << "Exemplo: arquivo.ppm" << endl;

        ptr_figura->setNome_arquivo(ptr->validaString());

        ptr->limparTela();

	nome = ptr_figura->getNome_arquivo();
        ptr_pixel = ptr_figura -> copiarInformacoes(nome);

        PretoBranco *ptr_peb = new PretoBranco;

        ptr_novo = ptr_peb->AplicarFiltro(ptr_figura, ptr_pixel,ptr_novo);
        ptr_peb->GravarArquivo(ptr_figura,ptr_novo);


}
       break;

case 3:
{
        ptr->limparTela();
        cout << "Digite o nome do arquivo que deseja abrir." << endl;
        cout << "(A imagem deve estar na pasta 'doc' deste programa)" << endl;
        cout << "Exemplo: arquivo.ppm" << endl;

        ptr_figura->setNome_arquivo(ptr->validaString());

        ptr->limparTela();

	nome = ptr_figura->getNome_arquivo();
        ptr_pixel = ptr_figura -> copiarInformacoes(nome);

        Polarizado *ptr_polarizado = new Polarizado;

        ptr_novo = ptr_polarizado->AplicarFiltro(ptr_figura, ptr_pixel,ptr_novo);
        ptr_polarizado->GravarArquivo(ptr_figura,ptr_novo);


}
       break;

case 4:
 {
        ptr->limparTela();
        cout << "Digite o nome do arquivo que deseja abrir." << endl;
        cout << "(A imagem deve estar na pasta 'doc' deste programa)" << endl;
        cout << "Exemplo: arquivo.ppm" << endl;

        ptr_figura->setNome_arquivo(ptr->validaString());

        ptr->limparTela();

	nome = ptr_figura->getNome_arquivo();
        ptr_pixel = ptr_figura -> copiarInformacoes(nome);

        Media *ptr_media = new Media;

        ptr_novo = ptr_media->AplicarFiltro(ptr_figura, ptr_pixel,ptr_novo);

        ptr->limparTela();

        ptr_media->GravarArquivo(ptr_figura,ptr_novo);


  }
       break;
case 5:
 {
	ptr->limparTela();
	cout << "Saindo do programa..." << endl;
   }
	break;
}

        delete(ptr_figura);
        delete(ptr_pixel);
        delete(ptr_novo);


return 0;

}

