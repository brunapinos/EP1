#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>

#include "figura.hpp"


#define Max 1020


using namespace std;


Figura:: Figura(){

	setTipo(tipo);
	setLargura(0);
	setAltura(0);
	setIntensidade_max(0);

}


string Figura:: getTipo(){
	return tipo;
}

void Figura:: setTipo(string tipo){
	this -> tipo = tipo;
}

int Figura:: getLargura(){
	return largura;
}

void Figura:: setLargura(int largura){
	this -> largura = largura;
}

int Figura:: getAltura(){
	return altura;
}

void Figura:: setAltura(int altura){
	this -> altura = altura;
}

void Figura:: setIntensidade_max(int intensidade){
	this -> intensidade = intensidade;
}

int Figura:: getIntensidade_max(){
	return intensidade;
}


void Figura:: setNome_arquivo(string nome_arquivo){
        this -> nome_arquivo = nome_arquivo;
}

string Figura:: getNome_arquivo(){
        return nome_arquivo;
}

Pixel* Figura:: copiarInformacoes(string nome_arquivo){
        ifstream ponteiro_arquivo;
        string arquivo = "./doc/" + nome_arquivo;
	ponteiro_arquivo.open(arquivo.c_str(), ios_base:: binary);

        if(!ponteiro_arquivo.is_open()){
		limparTela();
                cout << "Desculpe, erro ao abrir o arquivo." << endl;
		cout << "Verifique se o nome esta correto";
		cout << " ou se a imagem se encontra no diretorio deste programa." << endl;

                ponteiro_arquivo.close();
		exit(1);
          }

        else{
                cout << "Arquivo aberto." << endl;
         }


        string tipo, comentario, linha;
	std::streampos aux;
        int largura = 0, altura = 0, intensidade = 0;
	Pixel *ptr_pixel = new Pixel;
	char r,g,b,c;


        getline(ponteiro_arquivo,tipo);

        if(tipo != "P6"){
		limparTela();
                cout << "Desculpe, este arquivo nao e do formato esperado." << endl;
		cout << "Nao e um arquivo ppm tipo 'P6'." << endl;
                ponteiro_arquivo.close();
                exit(1);
        }

        else{ //faz nada
                }


	do{
		aux = ponteiro_arquivo.tellg();
		ponteiro_arquivo >> c;

		if(c == '#'){
			getline(ponteiro_arquivo,comentario);
		  }
		else{
	        	ponteiro_arquivo.seekg(aux);
	 	     }

	  } while(c == '#');


	ponteiro_arquivo >> largura;
	ponteiro_arquivo >> altura;
	ponteiro_arquivo >> intensidade;
	getline(ponteiro_arquivo,linha);

	ponteiro_arquivo.seekg(0,ios_base::cur);

	setTipo(tipo);
        setLargura(largura);
	setAltura(altura);
	setIntensidade_max(intensidade);


  for( int i=0; i < altura; ++i){
   for(int j=0; j < largura; ++j){
		ponteiro_arquivo.get(r);
		ptr_pixel->vermelho[i][j] = (unsigned char) r;
		ponteiro_arquivo.get(g);
        	ptr_pixel->verde[i][j] = (unsigned char)g;
		ponteiro_arquivo.get(b);
		ptr_pixel->azul[i][j] = (unsigned char)b;
   }
 }


	ponteiro_arquivo.close();

	return ptr_pixel;

}

