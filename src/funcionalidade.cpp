#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>

#include "funcionalidade.hpp"

using namespace std;


int Funcionalidade:: validaOpcao(int num_opcoes){

	int opcao, min = 1, max = num_opcoes;
	do{

    	    cin >> opcao;
	    if(opcao < min || opcao > max){
		cout << "Opcao invalida. Digite novamente. "<< endl;

		}
	   else {
		  return opcao;
		}
 	}
	while(1);
}


void Funcionalidade:: limparTela(){

int num_linhas = 25;

 for(int index = 0; index <= num_linhas; ++index){

	cout << "\n";

   }
}



string Funcionalidade:: validaString(){	

	string nome;
	cin >> nome;

	do{

		if(nome.size() == 0){
		   cout << "Nome Invalido. Digite Novamente" << endl;
		   cin >> nome;
		}
		else{
			while(nome.size() != 0 && (nome[nome.size()-1] == '\t' || 
			    nome[ nome.size()-1] == '\n' ||nome[ nome.size()-1] == ' ')){

				nome[nome.size()-1] = '\0';
			  }

			return nome;
  		}

	}
 while(1);

}


